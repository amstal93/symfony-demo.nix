{
  pkgs ? import <nixpkgs> { },
  PHP_VERSION ? "74",
  CI_REGISTRY
}:

let

  myphp = pkgs.${"php" + PHP_VERSION}.buildEnv {

    cgiSupport = false;
    fpmSupport = false;
    pharSupport = true; # needed by composer
    phpdbgSupport = false;
    systemdSupport = false;
    ipv6Support = false;
    valgrindSupport = false;
    apxs2Support = false;

    extensions = { all, ... }: with all; [
      pdo_sqlite
      json
      filter
      mbstring
      openssl
      tokenizer
      ctype
      dom
      session
      simplexml
      iconv
    ];
  };

  demo = pkgs.stdenv.mkDerivation {
    name = "demo-symfony";

    buildPhase = ''
      composer install --prefer-dist --optimize-autoloader
      bin/console cache:clear --no-warmup
    '';

    installPhase = ''
      cp -r . $out/
    '';

    src = builtins.fetchGit {
      url = "https://github.com/symfony/demo";
      ref = "v1.7.1";
      rev = "adf31163809dae0944a2424e50a774ab62addafa";
    };

    nativeBuildInputs = [
      myphp
      myphp.packages.composer
    ];
  };

in
{

  tarball = pkgs.stdenv.mkDerivation {
    name = "demo-symfony.tar.gz";
    dontBuild = true;
    src = demo;
    installPhase = "tar cfz $out .";
  };

  docker = pkgs.dockerTools.buildImage
    {
      name = "${CI_REGISTRY}/charlycoste/symfony-demo.nix/demo";
      config.Cmd = [ "${myphp}/bin/php" "-S" "0.0.0.0:8000" "-t" "${demo}/public" ];
    };
}
